// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2019
// Glenn P. Downing
// ----------------------------------


// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// Stuff we include
#include <iostream>   // istream, ostream
#include <vector>   // vector
#include <cmath>   // abs
#include <sstream>       // istringstream
#include <string>        // getline, string
#include <utility>       // make_pair, pair

using namespace std;
// ---------
// Allocator
// ---------

template <typename T, size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;}                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}

    public:
        // --------
        // typedefs
        // --------

        using      value_type = T;

        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;

    private:
        // ----
        // data
        // ----

        char a[N];

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * iterate through heap
         * check sentinels are right distance apart
         * check sentinels are equal
         * check we do not overrrun the end of the list
         * check we have no two free nodes in a row
         * check each block can hold at least one object
         */
        bool valid () const {
            iterator heap_iter((char*)a); 
            iterator end_iter((char*)(a + sizeof(a)));
            bool prev_is_free = false;
            while(heap_iter != end_iter) {
                if(heap_iter._p > end_iter._p) { return false; } // make sure we didn't go too far
                char* sentinel_header = heap_iter._p; // char pointer to header sentinel
                char* sentinel_footer = sentinel_header + sizeof(int) + abs(*heap_iter); // char pointer to footer sentinel
                int sentinel_header_val = *(int*)sentinel_header;
                int sentinel_footer_val = *(int*)sentinel_footer;
                if(sentinel_header_val != sentinel_footer_val) { return false; } // check sentinels are equal
                if(abs(sentinel_header_val) < (int)sizeof(value_type)) { return false; } // data block can hold at least one
                if(prev_is_free && sentinel_header_val > 0) { return false; } // check no two free in a row
                prev_is_free = sentinel_header_val > 0; // set bool for the next iteration
                ++heap_iter;
            }
            return true;}

    public:
        // --------
        // iterator
        // --------

        class iterator {

            friend class my_allocator;

            // -----------
            // operator ==
            // -----------

            friend bool operator == (const iterator& left, const iterator& right) {
                return left._p == right._p;}  // compare addresses

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs == rhs);}

            private:
                // ----
                // data
                // ----

                char* _p;

            public:
                // -----------
                // constructor
                // -----------

                iterator (char* p) : _p(p) {}

                // ----------
                // operator *
                // ----------

                int& operator * () const {
                    return *((int*)_p);}   //dereference _p, gives us value of the sentinel

                // -----------
                // operator ++
                // -----------

                iterator& operator ++ () {
                    int current_sentinel_val = abs(**this);
                    // sentinel size + block size + sentinel size
                    _p += sizeof(int) + current_sentinel_val + sizeof(int);
                    return *this;}

                // -----------
                // operator ++
                // -----------

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}};

        // -----------
        // constructor
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () {
            if(N < sizeof(T) + (2 * sizeof(int))) {
                throw bad_alloc();
            }
            int intial_sentinel_val = N - 8;
            int* header_sentinel = (int*) a;
            int* footer_sentinel = (int*) (a + N - sizeof(int));
            *header_sentinel = intial_sentinel_val; // set header sentinel
            *footer_sentinel = intial_sentinel_val; // set footer sentinel
            assert(valid());}

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate (size_type num_objects) {
            assert(valid());
            iterator heap_iter(a); 
            iterator end_iter(a + sizeof(a));
            int num_bytes = num_objects * sizeof(value_type);

            // while we are not at end, and the current block is not good
            // not good = negative or too small
            while((heap_iter != end_iter) && !(*heap_iter >= num_bytes)) {
                ++heap_iter;
            }
            if(heap_iter == end_iter) {
               throw bad_alloc(); 
            }
            char* sentinel_header = heap_iter._p; // char pointer to header sentinel
            char* sentinel_footer = sentinel_header + sizeof(int) + *heap_iter; // char pointer to footer sentinel
            pointer ret = (pointer) (sentinel_header + sizeof(int)); // point to first val of allocated space
            // if the block we found has less than enough room 
            // to hold this allocation, plus one more object (with sentinels)
            if(*heap_iter < (int)(num_bytes + sizeof(value_type) + 2*sizeof(int))) {
                *((int*)sentinel_header) *= -1;
                *((int*)sentinel_footer) *= -1;
            }
            // we gotta split
            else {
                int total_bytes = *heap_iter + 2*sizeof(int); // size of original block plus sentinels
                int new_chunk_bytes = num_bytes + 2*sizeof(int); // size of new chunk to make
                int leftover_chunk_bytes = total_bytes - new_chunk_bytes; // size of leftover chunk
                int leftover_chunk_sentinel = leftover_chunk_bytes - 2*sizeof(int); // value of sentinel in leftover chunk

                // set up sentinels for new chunk
                *((int*)(sentinel_header)) = num_bytes * -1; 
                sentinel_header += sizeof(int) + num_bytes;
                *((int*)(sentinel_header)) = num_bytes * -1;

                // set up sentinels for leftover chunk
                *((int*)(sentinel_footer)) = leftover_chunk_sentinel; 
                sentinel_footer -= leftover_chunk_sentinel;
                sentinel_footer -= sizeof(int);
                *((int*)(sentinel_footer)) = leftover_chunk_sentinel; 
            }
            assert(valid());
            return ret;}

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

        // ---------
        // coalesce
        // ---------

        void coalesce(pointer p) {
            char* sentinel_header = (char*)(p) - sizeof(int); // char pointer to header sentinel
            int sentinel_value = *((int*) sentinel_header);
            char* sentinel_footer = sentinel_header + sizeof(int) + sentinel_value; // char pointer to footer sentinel
            if(sentinel_header != a) { // not at beginning
                char* left_sentinel_footer = sentinel_header - sizeof(int);
                int left_sentinel_value = *((int*) left_sentinel_footer);
                if(left_sentinel_value > 0) { // only coalesce if free
                    sentinel_header -= (sizeof(int) + left_sentinel_value + sizeof(int)); // adjust header
                    sentinel_value += left_sentinel_value + 2*sizeof(int); // caluculate new val
                    *((int*)sentinel_header) = sentinel_value; // reset header
                    *((int*)sentinel_footer) = sentinel_value; // reset footer
                }
            }
            if((sentinel_footer + sizeof(int)) != (a + sizeof(a))) { // not at end
                char* right_sentinel_header = sentinel_footer + sizeof(int);
                int right_sentinel_value = *((int*) right_sentinel_header);
                if(right_sentinel_value > 0) { // only coalesce if free
                    sentinel_footer += (2*sizeof(int) + right_sentinel_value); // adjust footer
                    sentinel_value += right_sentinel_value + 2*sizeof(int); // calculate new val
                    *((int*)sentinel_header) = sentinel_value; // reset header
                    *((int*)sentinel_footer) = sentinel_value; // reset footer
                }
            }
        }

        // ----------
        // deallocate
        // ----------

        /** 
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * sets this block's sentinels to positive
         * coalesce to the left and right
         * checks to the left and creates one large block if necessary
         * checks to the right and creates one large block if necessary
         */
        void deallocate (pointer p, size_type s) {
            assert(valid());
            assert(s >= 0);
            char* sentinel_header = (char*)(p) - sizeof(int); // char pointer to header sentinel
            int sentinel_value = *((int*) sentinel_header) * -1;
            // assert(sentinel_value / sizeof(value_type) >= s);
            char* sentinel_footer = sentinel_header + sizeof(int) + sentinel_value; // char pointer to footer sentinel

            *((int*)(sentinel_header)) = sentinel_value;
            *((int*)(sentinel_footer)) = sentinel_value;

            coalesce(p);

            assert(valid());}

        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}};

// --------------
// read_operations
// --------------

vector<int> read_operations(istream &r) {
    vector<int> operations;
    stringstream op_string;
    string tmp_str;
    while(getline(r, tmp_str) && tmp_str != "") {
        op_string << tmp_str << " ";
    }
    int tmp_int;
    while(op_string >> tmp_int) {
        operations.push_back(tmp_int);
    }
    return operations;
}

// --------------
// do_operations
// --------------

vector<int> do_operations(const vector<int> &operations) {
    my_allocator<uint64_t, 1000> heap;

    for(const int &op : operations) {
        if(op >= 0) {
            heap.allocate(op);            
        }
        else {
            // Find the opth busy block
            my_allocator<uint64_t, 1000>::iterator heap_iter((char*)(&(heap[0])));
            char* dealloc_ptr;
            int busy_block_counter = 0;
            while(true) {
                if(*heap_iter < 0) {
                    ++busy_block_counter;
                }
                if(busy_block_counter == (op*-1)) {
                    dealloc_ptr = (char*)(&(*heap_iter)) + sizeof(int);
                    break;
                }
                ++heap_iter;
            }
            // get uint64_t pointer to start of alloced region
            heap.deallocate((uint64_t*) dealloc_ptr, *heap_iter / sizeof(uint64_t));
        }
    }


    my_allocator<uint64_t, 1000>::iterator heap_iter((char*)&heap[0]);
    my_allocator<uint64_t, 1000>::iterator end_iter( ((char*)&heap[999]) + 1 );

    vector<int> sentinels;
    while (heap_iter != end_iter) {
        sentinels.push_back(*heap_iter);
        ++heap_iter;
    }
    return sentinels;
 
}

// --------------
// print_sentinels
// --------------

void print_sentinels(ostream &w, const vector<int> &sentinels) {
    for(size_t i = 0; i < sentinels.size(); ++i) {
        w << sentinels[i] << " \n"[i == sentinels.size()-1];
    }
}

// --------------
// run_allocator
// --------------

void run_allocator(istream &r, ostream &w) {
    int num_tests;
    r >> num_tests;
    r.ignore(2);

    while(num_tests--) {
        vector<int> operations = read_operations(r);
        vector<int> sentinels = do_operations(operations);
        print_sentinels(w, sentinels);
    }
}


int main () {
    using namespace std;
    run_allocator(cin, cout);
    return 0;
}
