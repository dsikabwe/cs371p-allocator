// --------------
// RunAllocator.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.h"

// ----
// main
// ----

int main () {
    using namespace std;
    run_allocator(cin, cout);
    return 0;
}
