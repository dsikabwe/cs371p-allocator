// --------------
// Allocator.c++
// --------------

// ---------
// includes
// ---------

#include <cassert>       // assert
#include <iostream>      // endl, istream, ostream
#include <sstream>       // istringstream
#include <string>        // getline, string
#include <utility>       // make_pair, pair

// Things we are including

#include <vector>        // vector
#include <algorithm>     // sort

#include "Allocator.h"

using namespace std;

// --------------
// read_operations
// --------------

vector<int> read_operations(istream &r) {
    vector<int> operations;
    stringstream op_string;
    string tmp_str;
    while(getline(r, tmp_str) && tmp_str != "") {
        op_string << tmp_str << " ";
    }
    int tmp_int;
    while(op_string >> tmp_int) {
        operations.push_back(tmp_int);
    }
    return operations;
}

// --------------
// do_operations
// --------------

vector<int> do_operations(const vector<int> &operations) {
    my_allocator<uint64_t, 1000> heap;

    for(const int &op : operations) {
        if(op >= 0) {
            heap.allocate(op);
        }
        else {
            // Find the opth busy block
            my_allocator<uint64_t, 1000>::iterator heap_iter((char*)(&(heap[0])));
            char* dealloc_ptr;
            int busy_block_counter = 0;
            while(true) {
                if(*heap_iter < 0) {
                    ++busy_block_counter;
                }
                if(busy_block_counter == (op*-1)) {
                    dealloc_ptr = (char*)(&(*heap_iter)) + sizeof(int);
                    break;
                }
                ++heap_iter;
            }
            // get uint64_t pointer to start of alloced region
            heap.deallocate((uint64_t*) dealloc_ptr, *heap_iter / sizeof(uint64_t));
        }
    }


    my_allocator<uint64_t, 1000>::iterator heap_iter((char*)&heap[0]);
    my_allocator<uint64_t, 1000>::iterator end_iter( ((char*)&heap[999]) + 1 );

    vector<int> sentinels;
    while (heap_iter != end_iter) {
        sentinels.push_back(*heap_iter);
        ++heap_iter;
    }
    return sentinels;

}

// --------------
// print_sentinels
// --------------

void print_sentinels(ostream &w, const vector<int> &sentinels) {
    for(size_t i = 0; i < sentinels.size(); ++i) {
        w << sentinels[i] << " \n"[i == sentinels.size()-1];
    }
}

// --------------
// run_allocator
// --------------

void run_allocator(istream &r, ostream &w) {
    int num_tests;
    r >> num_tests;
    r.ignore(2);

    while(num_tests--) {
        vector<int> operations = read_operations(r);
        vector<int> sentinels = do_operations(operations);
        print_sentinels(w, sentinels);
    }
}