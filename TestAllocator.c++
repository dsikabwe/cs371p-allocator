// ----------------------------------------
// projects/c++/allocator/TestAllocator.c++
// Copyright (C) 2019
// Glenn P. Downing
// ----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

// things we include

#include <sstream>

#include "gtest/gtest.h"

#include "Allocator.h"

TEST(Allocator_REPL_Tester, test_read_1) {
    istringstream input("5\n\n");
    vector<int> operations = read_operations(input);
    vector<int> solution = {5};
    ASSERT_TRUE(equal(begin(operations), end(operations), begin(solution)));
}

TEST(Allocator_REPL_Tester, test_read_2) {
    istringstream input("5\n3\n3\n\n");
    vector<int> operations = read_operations(input);
    vector<int> solution = {5,3,3};
    ASSERT_TRUE(equal(begin(operations), end(operations), begin(solution)));
}

TEST(Allocator_REPL_Tester, test_read_3) {
    istringstream input("5\n3\n3\n-1\n-1\n");
    vector<int> operations = read_operations(input);
    vector<int> solution = {5,3,3,-1,-1};
    ASSERT_TRUE(equal(begin(operations), end(operations), begin(solution)));
}

TEST(Allocator_REPL_Tester, test_do_1) {
    vector<int> operations = {5};
    vector<int> sentinels = do_operations(operations);
    vector<int> solution = {-40, 944};
    ASSERT_TRUE(equal(begin(sentinels), end(sentinels), begin(solution)));
}

TEST(Allocator_REPL_Tester, test_do_2) {
    vector<int> operations = {5,3};
    vector<int> sentinels = do_operations(operations);
    vector<int> solution = {-40, -24, 912};
    ASSERT_TRUE(equal(begin(sentinels), end(sentinels), begin(solution)));
}

TEST(Allocator_REPL_Tester, test_do_3) {
    vector<int> operations = {5,3,-1};
    vector<int> sentinels = do_operations(operations);
    vector<int> solution = {40, -24, 912};
    ASSERT_TRUE(equal(begin(sentinels), end(sentinels), begin(solution)));
}

TEST(Allocator_REPL_Tester, test_do_4) {
    vector<int> operations = {5,3,3,-1,-1};
    vector<int> sentinels = do_operations(operations);
    vector<int> solution = {72,-24,880};
    ASSERT_TRUE(equal(begin(sentinels), end(sentinels), begin(solution)));
}

TEST(Allocator_REPL_Tester, test_print_1) {
    ostringstream w;
    vector<int> sentinels = {-40, 944};
    print_sentinels(w, sentinels);
    string solution = "-40 944\n";
    ASSERT_EQ(w.str(), solution);
}

TEST(Allocator_REPL_Tester, test_print_2) {
    ostringstream w;
    vector<int> sentinels = {72, -24, 880};
    print_sentinels(w, sentinels);
    string solution = "72 -24 880\n";
    ASSERT_EQ(w.str(), solution);
}

TEST(Allocator_REPL_Tester, test_run_1) {
    istringstream input (   "2\n"
                            "\n"
                            "5\n"
                            "\n"
                            "5\n"
                            "3\n"
                        );
    ostringstream output;
    string solution = "-40 944\n"
                      "-40 -24 912\n";
    run_allocator(input, output);
    ASSERT_EQ(output.str(), solution);
}

TEST(Allocator_REPL_Tester, test_run_2) {
    istringstream input (   "4\n"
                            "\n"
                            "5\n"
                            "\n"
                            "5\n"
                            "3\n"
                            "\n"
                            "5\n"
                            "3\n"
                            "-1\n"
                            "\n"
                            "5\n"
                            "3\n"
                            "3\n"
                            "-1\n"
                            "-1\n"
                        );
    ostringstream output;
    string solution = "-40 944\n"
                      "-40 -24 912\n"
                      "40 -24 912\n"
                      "72 -24 880\n";
    run_allocator(input, output);
    ASSERT_EQ(output.str(), solution);
}

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;

    const pointer    p = x.allocate(s);

    int* tmp = (int*)p;
    tmp -= 1;

    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

TEST(TestAllocator2, const_index) {
    const my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}

TEST(TestAllocator2, index) {
    my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}

// --------------
// TestAllocator3
// --------------

template <typename A>
struct TestAllocator3 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    100>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator3, my_types_2);

TYPED_TEST(TestAllocator3, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator3, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}
